﻿using System;
using System.Buffers.Binary;
using System.Collections.Generic;
using System.Device.I2c;
using System.Text;


namespace EonData.StormyPi.Hardware {
    class ConnectI2C : IDisposable {
        private I2cDevice i2cd;
        public void Dispose() => ((IDisposable)i2cd).Dispose();

        public ConnectI2C(int busId, int address) {
            i2cd = I2cDevice.Create(new I2cConnectionSettings(busId, address));
        }

        public byte Read8BitsFromRegister(byte register) {
            i2cd.WriteByte(register);
            byte rVal = i2cd.ReadByte();
            return rVal;
        }

        public ushort Read16BitsFromRegister(byte register, bool useBigEndian = false) {
            var r = ReadBitsFromRegister(register, 2);

            if (useBigEndian) {
                return BinaryPrimitives.ReadUInt16BigEndian(r);
            }
            else {
                return BinaryPrimitives.ReadUInt16LittleEndian(r);
            }
        }

        public void WriteToRegister(byte register, byte value) {
            Span<byte> command = stackalloc[] { register, value };
            i2cd.Write(command);
        }

        private Span<byte> ReadBitsFromRegister(byte register, int sizeBits) {
            Span<byte> r = new byte[sizeBits];
            i2cd.WriteByte(register);
            i2cd.Read(r);
            return r;
        }
    }
}
