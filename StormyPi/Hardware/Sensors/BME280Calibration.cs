﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EonData.StormyPi.Hardware.Sensors {
     class BME280Calibration {
        public ushort T1 { get; private set; }
        public short T2 { get; private set; }
        public short T3 { get; private set; }

        public ushort P1 { get; private set; }
        public short P2 { get; private set; }
        public short P3 { get; private set; }
        public short P4 { get; private set; }
        public short P5 { get; private set; }
        public short P6 { get; private set; }
        public short P7 { get; private set; }
        public short P8 { get; private set; }
        public short P9 { get; private set; }

        public byte H1 { get; private set; }
        public short H2 { get; private set; }
        public byte H3 { get; private set; }
        public short H4 { get; private set; }
        public short H5 { get; private set; }
        public sbyte H6 { get; private set; }

        public BME280Calibration(ConnectI2C i2cBME280) {
            ReadTemperatureCalibrationData(i2cBME280);
            ReadHumidityCalibrationData(i2cBME280);
            ReadPressureCalibrationData(i2cBME280);
        }

        private void ReadTemperatureCalibrationData(ConnectI2C i2c) {
            T1 = i2c.Read16BitsFromRegister(BME280Registers.DIG_T1);
            T2 = (short)i2c.Read16BitsFromRegister(BME280Registers.DIG_T2);
            T3 = (short)i2c.Read16BitsFromRegister(BME280Registers.DIG_T3);
        }

        private void ReadHumidityCalibrationData(ConnectI2C i2c) {
            P1 = i2c.Read16BitsFromRegister(BME280Registers.DIG_P1);
            P2 = (short)i2c.Read16BitsFromRegister(BME280Registers.DIG_P2);
            P3 = (short)i2c.Read16BitsFromRegister(BME280Registers.DIG_P3);
            P4 = (short)i2c.Read16BitsFromRegister(BME280Registers.DIG_P4);
            P5 = (short)i2c.Read16BitsFromRegister(BME280Registers.DIG_P5);
            P6 = (short)i2c.Read16BitsFromRegister(BME280Registers.DIG_P6);
            P7 = (short)i2c.Read16BitsFromRegister(BME280Registers.DIG_P7);
            P8 = (short)i2c.Read16BitsFromRegister(BME280Registers.DIG_P8);
            P9 = (short)i2c.Read16BitsFromRegister(BME280Registers.DIG_P9);
        }

        private void ReadPressureCalibrationData(ConnectI2C i2c) {
            H1 = i2c.Read8BitsFromRegister(BME280Registers.DIG_H1);
            H2 = (short)i2c.Read16BitsFromRegister(BME280Registers.DIG_H2);
            H3 = i2c.Read8BitsFromRegister(BME280Registers.DIG_H3);
            byte h4a = i2c.Read8BitsFromRegister(BME280Registers.DIG_H4);
            byte h4b = i2c.Read8BitsFromRegister(BME280Registers.DIG_H4 + 1);
            H4 = (short)((h4a << 4) | (h4b & 0xF));
            byte h5a = i2c.Read8BitsFromRegister(BME280Registers.DIG_H5 + 1);
            byte h5b = i2c.Read8BitsFromRegister(BME280Registers.DIG_H5);
            H5 = (short)((h5a << 4) | (h5b >> 4));
            H6 = (sbyte)i2c.Read8BitsFromRegister(BME280Registers.DIG_H6);
        }
    }
}
