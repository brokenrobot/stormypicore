﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EonData.StormyPi.Hardware.Sensors {
    public struct BME280Status {
        public bool IsMeasuring { get; private set; }
        public bool IsImageUpdating { get; private set; }

        internal BME280Status(byte status) {
            if ((status & 1) == 1) {
                IsImageUpdating = true;
            }
            else {
                IsImageUpdating = false;
            }

            if (((status >> 3) & 1) == 1) {
                IsMeasuring = true;
            }
            else {
                IsMeasuring = false;
            }
        }
    }
}