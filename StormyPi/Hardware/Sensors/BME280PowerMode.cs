﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EonData.StormyPi.Hardware.Sensors {
    public enum BME280PowerMode {
        Sleep,
        Normal,
        Forced
    }
}