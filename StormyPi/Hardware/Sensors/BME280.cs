﻿using System;
using System.Collections.Generic;
using System.Device.I2c;
using System.Text;

namespace EonData.StormyPi.Hardware.Sensors {
    public sealed class BME280 : IDisposable {
        public const int DEFAULT_I2C_DEVICE_ADDRESS = 0x77;
        public const byte EXPECTED_DEVICE_ID = 0x60;

        public BME280PowerMode PowerMode { get; set; }

        private ConnectI2C i2c;

        private BME280Calibration calibration;

        public void Dispose() => ((IDisposable)i2c).Dispose();

        public BME280(int busId = 1) {
            i2c = new ConnectI2C(busId, DEFAULT_I2C_DEVICE_ADDRESS);

            //check device id
            byte devId = i2c.Read8BitsFromRegister(BME280Registers.CHIPID);
            if (devId != EXPECTED_DEVICE_ID) {
                throw new System.IO.IOException($"The device ID found ({devId}) does not match the expected value ({EXPECTED_DEVICE_ID}) for a BME280 chip.");
            }

            calibration = new BME280Calibration(i2c);
        }

        public BME280Status ReadStatus() => new BME280Status(i2c.Read8BitsFromRegister(BME280Registers.STATUS));

        public void Reset() {
            const byte RESET_COMMAND = 0xB6;
            i2c.WriteToRegister(BME280Registers.RESET, RESET_COMMAND);
        }
    }
}
