﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EonData.StormyPi.Hardware.Sensors {
    /// <summary>
    /// Contains constant values for the register addresses of the BME280 sensor.
    /// </summary>
    public static class BME280Registers {
        /*
         * device info registers
         */
        /// <summary>
        /// Stores the chip identification number.
        /// </summary>
        public const byte CHIPID = 0xD0;

        /// <summary>
        /// Contains two bits which indicate the status of the device.
        /// </summary>
        public const byte STATUS = 0xF3;

        /*
         * device configuration and control registers
         */
        /// <summary>
        /// Used to reset the chip's configuration.
        /// </summary>
        public const byte RESET = 0xE0;

        /// <summary>
        /// Sets the rate, filter and interface options of the device. Writes to this register in normal mode may be ignored. In sleep mode writes are not ignored.
        /// </summary>
        public const byte CONFIG = 0xF5;

        /// <summary>
        /// Sets the pressure and temperature data acquisition options of the device. The register needs to be written after changing <see cref="CTRL_HUM"/> for the changes to become effective.
        /// </summary>
        public const byte CTRL_MEAS = 0xF4;

        /// <summary>
        /// Sets the humidity data acquisition options of the device. Changes to this register only become effective after a write operation to <see cref="CTRL_MEAS"/>
        /// </summary>
        public const byte CTRL_HUM = 0xF2;

        /*
         * measurement registers
         */
        public const byte HUM_LSB = 0xFE;

        public const byte HUM_MSB = 0xFD;

        public const byte TEMP_XLSB = 0xFC;

        public const byte TEMP_LSB = 0xFB;

        public const byte TEMP_MSB = 0xFA;

        public const byte PRESS_XLSB = 0xF9;
        
        public const byte PRESS_LSB = 0xF8;

        public const byte PRESS_MSB = 0xF7;

        

        /*
         * temperature calibration registers
         */
        /// <summary>
        /// Temperature calibration data T1 (unsigned short)
        /// </summary>
        public const byte DIG_T1 = 0x88;

        /// <summary>
        /// Temperature calibration data T2 (signed short)
        /// </summary>
        public const byte DIG_T2 = 0x8A;

        /// <summary>
        /// Temperature calibration data T3 (signed short)
        /// </summary>
        public const byte DIG_T3 = 0x8C;

        /*
         * pressure calibration registers
         */
        /// <summary>
        /// Pressure calibration data P1 (unsigned short)
        /// </summary>
        public const byte DIG_P1 = 0x8E;

        /// <summary>
        /// Pressure calibration data P2 (signed short)
        /// </summary>
        public const byte DIG_P2 = 0x90;

        /// <summary>
        /// Pressure calibration data P3 (signed short)
        /// </summary>
        public const byte DIG_P3 = 0x92;

        /// <summary>
        /// Pressure calibration data P4 (signed short)
        /// </summary>
        public const byte DIG_P4 = 0x94;

        /// <summary>
        /// Pressure calibration data P5 (signed short)
        /// </summary>
        public const byte DIG_P5 = 0x96;

        /// <summary>
        /// Pressure calibration data P6 (signed short)
        /// </summary>
        public const byte DIG_P6 = 0x98;

        /// <summary>
        /// Pressure calibration data P7 (signed short)
        /// </summary>
        public const byte DIG_P7 = 0x9A;

        /// <summary>
        /// Pressure calibration data P8 (signed short)
        /// </summary>
        public const byte DIG_P8 = 0x9C;

        /// <summary>
        /// Pressure calibration data P9 (signed short)
        /// </summary>
        public const byte DIG_P9 = 0x9E;

        /*
         * humidity calibration registers
         */
        /// <summary>
        /// Humidity calibration data H1 (unsigned char)
        /// </summary>
        public const byte DIG_H1 = 0xA1;

        /// <summary>
        /// Humidity calibration data H2 (signed short)
        /// </summary>
        public const byte DIG_H2 = 0xE1;

        /// <summary>
        /// Humidity calibration data H3 (unsigned char)
        /// </summary>
        public const byte DIG_H3 = 0xE3;

        /// <summary>
        /// Humidity calibration data H4 (signed short)
        /// </summary>
        public const byte DIG_H4 = 0xE4;

        /// <summary>
        /// Humidity calibration data H5 (signed short)
        /// </summary>
        public const byte DIG_H5 = 0xE5;

        /// <summary>
        /// Humidity calibration data H6 (signed char)
        /// </summary>
        public const byte DIG_H6 = 0xE7;
    }
}
